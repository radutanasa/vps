var config = require('./config');
var kafka = require('kafka-node');
var winston = require('winston');

var Connector = (function() {

    // kafka specific implementation
    var KafkaConnector = function() {

        var HighLevelProducer = kafka.HighLevelProducer;
        var client = new kafka.Client(config.mq.kafka.connectionString);
        var producer = new HighLevelProducer(client);

        producer.on('error', function (err) {
            winston.log('error', err.message);
        });

        function produce(content) {
            var payloads = [{ topic: config.mq.kafka.topic, messages: content }];
            producer.send(payloads, function(err, data) {
                if (err) {
                    winston.log('error', err.message);
                }
            });
        }

        return {
            produce:produce
        }
    }

    return {
        Kafka: KafkaConnector
    }
})();

module.exports = Connector;