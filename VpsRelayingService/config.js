var config = {};

config.log = {
    level: 'error',
    filename: '/var/log/vps/vps-relaying-service.log',
    zippedArchive: true
};

config.mq = {
    kafka: {
        connectionString: '127.0.0.1:2181',
        topic: 'telemetry'
    }
};

module.exports = config;