var config = require('./config')
var express = require('express');
var bodyParser = require('body-parser');

var Connector = require('./connector');
var winston = require('winston');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));

winston.add(winston.transports.DailyRotateFile, config.log);
winston.remove(winston.transports.Console);

var mqConnector = new Connector.Kafka();

// used to check whether the instance is healthy
app.get('/healthcheck', function(req, res) {
    res.sendStatus(200);
});

// receives sensor data
app.post('/save', function(req, res) {
    var content = JSON.stringify(req.body);
    mqConnector.produce(content);
    res.sendStatus(200);
});

// production error handler
app.use(function(err, req, res, next) {
    res.sendStatus(err.status || 500);
    winston.log('error', err.message);
});

module.exports = app;
