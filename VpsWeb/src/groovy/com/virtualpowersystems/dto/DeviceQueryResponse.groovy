package com.virtualpowersystems.dto

class DeviceQueryResponse {

    def deviceList = []
    Long deviceCount = 0
}