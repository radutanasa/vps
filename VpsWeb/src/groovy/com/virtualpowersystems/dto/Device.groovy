package com.virtualpowersystems.dto

class Device {

    String deviceId
    String timestamp
    String payload
    boolean online

}