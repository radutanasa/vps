package com.virtualpowersystems.service

import com.virtualpowersystems.dto.Device
import com.virtualpowersystems.dto.DeviceQueryResponse
import org.springframework.beans.factory.InitializingBean
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisSentinelPool
import redis.clients.jedis.Pipeline
import redis.clients.jedis.Response

class DeviceService implements InitializingBean {

    boolean transactional = false

    def grailsApplication
    JedisSentinelPool jedisSentinelPool

    DeviceQueryResponse load(int offset, int max) {
        Jedis jedis = null
        try {
            jedis = jedisSentinelPool.getResource()
            Pipeline pipeline = jedis.pipelined()
            Response<Long> deviceCountResponse = pipeline.zcard(grailsApplication.config.jedis.deviceSet.key)
            Response<Set> deviceIdListResponse = pipeline.zrange(grailsApplication.config.jedis.deviceSet.key, offset, offset + max - 1)
            pipeline.sync()

            DeviceQueryResponse deviceQueryResponse = new DeviceQueryResponse()
            deviceQueryResponse.deviceCount = deviceCountResponse.get()
            def deviceIdList = deviceIdListResponse.get().toArray() as List

            pipeline = jedis.pipelined()
            def deviceMapResponseList = []
            deviceIdList.each { deviceId ->
                Response<Map<String,String>> deviceMapResponse = pipeline.hgetAll("${grailsApplication.config.jedis.sensorMap.keyPrefix}${deviceId}")
                deviceMapResponseList.add(deviceMapResponse)
            }
            pipeline.sync()

            deviceMapResponseList.each { Response<Map<String, String>> deviceMapResponse ->
                Map<String, String> deviceMap = deviceMapResponse.get()
                Device device = new Device()
                device.deviceId = deviceMap.get("deviceId")
                device.timestamp = deviceMap.get("timestamp")
                device.payload = deviceMap.get("payload")
                device.online = System.currentTimeMillis() - Long.valueOf(device.timestamp) < grailsApplication.config.offlineThreshold
                deviceQueryResponse.deviceList.add(device)
            }
            return deviceQueryResponse
        } finally {
            if (jedis != null) {
                jedis.close()
            }
        }
    }

    @Override
    void afterPropertiesSet() throws Exception {
        jedisSentinelPool = new JedisSentinelPool(grailsApplication.config.jedis.masterName,
                grailsApplication.config.jedis.sentinels);
    }
}