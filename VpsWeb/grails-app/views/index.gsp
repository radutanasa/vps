<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>VPS Dashboard</title>
</head>
<body>
    <div ng-app="vps" ng-controller="DeviceController" class="container">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="30%">Device ID</th>
                    <th width="20%">Timestamp</th>
                    <th>Payload</th>
                    <th width="10%">Status</th>
                </tr>
            </thead>
            <tbody>
                <tr dir-paginate="device in deviceList | itemsPerPage: devicesPerPage" total-items="deviceCount" current-page="pagination.current">
                    <td>{{ device.deviceId }}</td>
                    <td>{{ device.timestamp }}</td>
                    <td>{{ device.payload}}</td>
                    <td>
                        <img src="/assets/online.png" ng-if="device.online"/>
                        <img src="/assets/offline.png" ng-if="!device.online"/>
                    </td>
                </tr>
            </tbody>
        </table>
        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)"></dir-pagination-controls>
    </div>
    <script type="text/javascript">
        Vps.init();
    </script>
</body>
</html>
