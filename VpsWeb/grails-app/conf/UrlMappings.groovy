class UrlMappings {

	static mappings = {
        "/device/$action" {
            controller = "device"
        }
        "/"(view:"/index")
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
	}
}
