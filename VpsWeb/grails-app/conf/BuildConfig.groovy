grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.7
grails.project.source.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.fork = [
        // configure settings for the run-app JVM
        run: [maxMemory: 2048, minMemory: 2048, debug: false, maxPerm: 512, forkReserve:false],
        // configure settings for the run-war JVM
        war: [maxMemory: 2048, minMemory: 2048, debug: false, maxPerm: 512, forkReserve:false]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
        mavenRepo "http://repo.grails.org/grails/repo"
        mavenRepo "http://mirrors.ibiblio.org/pub/mirrors/maven2"
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://ftp.us.xemacs.org/pub/mirrors/maven2"
        mavenRepo "http://repo.exist.com/maven2"
        mavenRepo "http://maven.ala.org.au/repository"
    }

    dependencies {
        compile "redis.clients:jedis:2.7.4-SNAPSHOT"
    }

    plugins {
        // plugins for the build system only
        build ":tomcat:7.0.55.2" // or ":tomcat:8.0.20"

        // plugins for the compile step
        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.8'
        compile ":asset-pipeline:2.1.5"

        // plugins needed at runtime but not for compilation
        runtime ":database-migration:1.4.0"
        runtime ":jquery:1.11.1"
    }
}

grails.war.resources = { stagingDir ->
    File libDir = new File(stagingDir, 'WEB-INF/lib')

    def deleteJars = { jarNameStart ->
        libDir.eachFile { file ->
            if (file.name.startsWith(jarNameStart)) {
                file.delete()
            }
        }
    }

    deleteJars 'servlet-api'
}