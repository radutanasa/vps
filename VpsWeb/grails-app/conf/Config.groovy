grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false
// log request parameters
grails.exceptionresolver.logRequestParameters = true
// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

grails.databinding.convertEmptyStringsToNull = true
grails.databinding.trimStrings = true
grails.databinding.dateFormats = ["MM/dd/yyyy HH:mm", "MM/dd/yyyy"]

// log4j configuration
log4j = {
    appenders {
        appender new org.apache.log4j.DailyRollingFileAppender(name:"vps-web", layout:pattern(conversionPattern: '%d [%-5p] [%c] - %m%n'), fileName:"/tmp/vps-web.log")
        console name:'stdout', layout:pattern(conversionPattern: '%d [%-5p] [%c] - %m%n')
    }

    root {
        debug 'stdout'
        error 'vivahub-err'
    }
}

grails.assets.minifyJs = false

// in millis
offlineThreshold = 10000

environments {
    development {
        jedis.sentinels = ["127.0.0.1:26379", "127.0.0.1:26479", "127.0.0.1:26579"] as Set
        jedis.masterName = "vps_master"
        jedis.sensorMap.keyPrefix = "device:"
        jedis.deviceSet.key = "deviceSet"
    }
    production {
        jedis.sentinels = ["172.30.0.5:26379", "172.30.0.54:26479", "172.30.0.133:26579"] as Set
        jedis.masterName = "vps_master"
        jedis.sensorMap.keyPrefix = "device:"
        jedis.deviceSet.key = "deviceSet"
    }
}
