Vps = (function() {

    function DeviceController($scope, $http, $interval) {

        $scope.refreshInterval = 5000;
        $scope.deviceList = [];
        $scope.deviceCount = 0;
        $scope.devicesPerPage = 10;

        $scope.pagination = {
            current: 1
        }

        $scope.pageChanged = function(newPage) {
            loadDevices(newPage);
        }

        function loadDevices(pageNumber) {
            $http.get("/device/list?page=" + pageNumber)
                .success(function(data) {
                    $scope.deviceList = data.deviceList;
                    $scope.deviceCount = data.deviceCount;
                });
        }

        loadDevices(1);

        $interval(function() {
            loadDevices($scope.pagination.current);
        }, $scope.refreshInterval);
    }

    function init() {
        var app = angular.module('vps', ['angularUtils.directives.dirPagination']);
        app.config(function(paginationTemplateProvider) {
            paginationTemplateProvider.setPath('/templates/dirPagination.tpl.html');
        });
        app.controller('DeviceController', DeviceController);
    }

    return {
        init:init
    }

})();
