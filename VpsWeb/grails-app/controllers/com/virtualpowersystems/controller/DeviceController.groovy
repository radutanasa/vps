package com.virtualpowersystems.controller

import com.virtualpowersystems.dto.DeviceQueryResponse
import grails.converters.JSON

class DeviceController {

    def deviceService

    static int max = 10

    def list() {
        int offset = (params.page.toInteger() - 1) * max
        DeviceQueryResponse deviceQueryResponse = deviceService.load(offset, max)
        def resp = ["deviceList":deviceQueryResponse.deviceList, "deviceCount":deviceQueryResponse.deviceCount]
        render resp as JSON
    }
}