package com.virtualpowersystems.spout;


import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Sensor telemetry data spout used for mocking the feed of telemetry events.
 */
public class SensorTelemetryDataMockSpout extends BaseRichSpout {
    SpoutOutputCollector _collector;
    final String[] deviceIds = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t"};
    Random _rand;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        _collector = collector;
        _rand = new Random();
    }

    @Override
    public void nextTuple() {
        Utils.sleep(300);
        String deviceId = deviceIds[_rand.nextInt(deviceIds.length)];
        _collector.emit(new Values(deviceId, Long.toString(System.currentTimeMillis()), UUID.randomUUID().toString()));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("deviceId", "timestamp", "payload"));
    }
}
