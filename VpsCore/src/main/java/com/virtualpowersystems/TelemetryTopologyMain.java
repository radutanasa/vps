package com.virtualpowersystems;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import com.virtualpowersystems.bolt.CacheUpdaterBolt;
import com.virtualpowersystems.bolt.CassandraStorageBolt;
import com.virtualpowersystems.bolt.DecoderBolt;
import com.virtualpowersystems.bolt.TelemetryStatsAggregatorBolt;
import org.yaml.snakeyaml.Yaml;
import storm.kafka.*;

import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

public class TelemetryTopologyMain {

    public static void main(String[] args) throws Exception {

        String environment = System.getProperty("env");
        if (environment == null) {
            environment = "production";
        }
        InputStream in = TelemetryTopologyMain.class.getClassLoader().getResourceAsStream("config_" + environment + ".yml");
        Yaml yaml = new Yaml();
        Map configMap = yaml.loadAs(in, Map.class);
        Config conf = new Config();
        conf.putAll(configMap);

        BrokerHosts hosts = new ZkHosts((String)conf.get("zookeeper.host"));
        String topic = (String)conf.get("kafka.topic");
        SpoutConfig spoutConfig = new SpoutConfig(hosts, topic, "/" + topic, UUID.randomUUID().toString());
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("sensorTelemetrySpout", kafkaSpout, 3);
        builder.setBolt("decoder", new DecoderBolt(), 3).shuffleGrouping("sensorTelemetrySpout");
        builder.setBolt("telemetryStatsAggregator", new TelemetryStatsAggregatorBolt(), 3)
                .fieldsGrouping("decoder", new Fields("deviceId"));
        builder.setBolt("cacheUpdater", new CacheUpdaterBolt(), 3)
                .shuffleGrouping("telemetryStatsAggregator");
        builder.setBolt("cassandraStorage", new CassandraStorageBolt(), 3)
                .shuffleGrouping("decoder");

        StormSubmitter.submitTopology("telemetryTopology", conf, builder.createTopology());
    }
}
