package com.virtualpowersystems.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.Pipeline;

import java.io.InputStream;
import java.util.*;

/**
 * Bolt used to write aggregated telemetry data into a persistent cache.
 * Created by bogdanvlad on 9/10/15.
 */
public class CacheUpdaterBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(CacheUpdaterBolt.class);
    private OutputCollector outputCollector;
    private Map<String, String> config;
    private JedisSentinelPool jedisSentinelPool = null;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        outputCollector = collector;
        config = (Map<String, String>)stormConf;
        Set<String> sentinels = new HashSet<String>();
        String[] sentinelHostPortValues = config.get("jedis.sentinels").split(",");
        for (String sentinelHostPortValue : sentinelHostPortValues) {
            sentinels.add(sentinelHostPortValue);
        }

        jedisSentinelPool = new JedisSentinelPool(config.get("jedis.masterName"), sentinels);
    }

    @Override
    public void execute(Tuple input) {
        try (Jedis jedis = jedisSentinelPool.getResource()) {
            String deviceId = (String) input.getValueByField("deviceId");
            String timestamp = (String) input.getValueByField("timestamp");
            String payload = (String) input.getValueByField("payload");

            Map<String, String> values = new HashMap<String, String>();
            values.put("deviceId", deviceId);
            values.put("timestamp", timestamp);
            values.put("payload", payload);

            // only update the set if it's a new device
            Double score = jedis.zscore(config.get("jedis.deviceSet.key"), deviceId);
            if (score == null) {
                // we create a transaction
                Pipeline pipeline = jedis.pipelined();
                pipeline.multi();
                pipeline.zadd(config.get("jedis.deviceSet.key"), Long.valueOf(timestamp), deviceId);
                pipeline.hmset(config.get("jedis.sensorMap.keyPrefix") + deviceId, values);
                pipeline.exec();
            } else {
                jedis.hmset(config.get("jedis.sensorMap.keyPrefix") + deviceId, values);
            }
        } catch (Exception ex) {
            logger.error("Exception while updating cache with tuple " + input, ex);
        } finally {
            outputCollector.ack(input);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}
