package com.virtualpowersystems.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Very basic decoder used to transform a kafka byte message into a domain-specific telemetry tuple.
 * Created by bogdanvlad on 9/10/15.
 */
public class DecoderBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(DecoderBolt.class);
    private static JSONParser jsonParser = new JSONParser();
    private OutputCollector outputCollector;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        outputCollector = collector;
    }

    @Override
    public void execute(Tuple input) {
        // process the kafka string to a topology-interpretable tuple = (deviceId, timestamp, payload)
        try {
            JSONObject obj = (JSONObject)jsonParser.parse(input.getString(0));
            String deviceId = (String)obj.get("deviceId");
            String timestamp = ((Long)obj.get("timestamp")).toString();
            String payload = (String)obj.get("payload");
            // emit tuple for each decoded message. anchor it to the previous tuple
            outputCollector.emit(input, new Values(deviceId, timestamp, payload));
        } catch (ParseException ex) {
            // log the error
            logger.error("Parsing error on decoding input string = " + input.getString(0), ex);
        } finally {
            // make sure we ack the tuple
            outputCollector.ack(input);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("deviceId", "timestamp", "payload"));
    }

}
