package com.virtualpowersystems.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DowngradingConsistencyRetryPolicy;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.policies.RoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Bolt used to persist individual telemetry data.
 * Created by radu on 10/09/15.
 */
public class CassandraStorageBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(CassandraStorageBolt.class);
    private OutputCollector outputCollector;
    private Cluster cassandra;
    private Session session;
    private Random random = new Random();
    private PreparedStatement statement;

    @Override
    public void prepare(Map stormConf, TopologyContext topologyContext, OutputCollector outputCollector) {
        this.outputCollector = outputCollector;
        Map<String, String> config = (Map<String, String>)stormConf;
        cassandra = Cluster.builder()
                .addContactPoints(config.get("cassandra.nodes").split(","))
                .withRetryPolicy(DowngradingConsistencyRetryPolicy.INSTANCE)
                .withReconnectionPolicy(new ExponentialReconnectionPolicy(100L, TimeUnit.MINUTES.toMillis(5)))
                .withLoadBalancingPolicy(new TokenAwarePolicy(new RoundRobinPolicy()))
                .build();
        session = cassandra.connect(config.get("cassandra.keyspace"));
        statement = session.prepare(config.get("cassandra.cql"));
    }

    @Override
    public void execute(Tuple tuple) {
        try {
            BoundStatement bound = statement.bind(
                    tuple.getValueByField("deviceId") + "-" + tuple.getValueByField("timestamp") + "-" + Math.abs(random.nextInt()),
                    tuple.getValueByField("deviceId"),
                    Long.valueOf((String)tuple.getValueByField("timestamp")),
                    tuple.getValueByField("payload"));
            session.execute(bound);
            outputCollector.ack(tuple);
        } catch (Exception ex) {
            logger.error("Exception while inserting telemetry data for tuple " + tuple, ex);
            // and fail the tuple for the spout to retry the processing
            outputCollector.fail(tuple);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }

    @Override
    public void cleanup() {
        cassandra.close();
    }
}
