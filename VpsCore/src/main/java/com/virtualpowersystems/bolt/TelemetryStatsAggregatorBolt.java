package com.virtualpowersystems.bolt;

import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.*;

/**
 * Aggregates telemetry data that will be sent to the cache updater bolt.
 *
 * Created by bogdanvlad on 9/10/15.
 */
public class TelemetryStatsAggregatorBolt extends BaseRichBolt {

    private Map<String, String[]> stateMap;
    private OutputCollector outputCollector;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        stateMap = new LinkedHashMap<String, String[]>();
        outputCollector = collector;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!isTickTuple(tuple)) {
            // execute basic processing
            if (stateMap.containsKey(tuple.getStringByField("deviceId"))) {
                String[] values = stateMap.get(tuple.getStringByField("deviceId"));
                if ((Long.valueOf(values[0]) < Long.valueOf(tuple.getStringByField("timestamp")))) {
                    String[] newValues = new String[]{tuple.getStringByField("timestamp"), tuple.getStringByField("payload")};
                    stateMap.put(tuple.getStringByField("deviceId"), newValues);
                }
            } else {
                // new entry for this device
                String[] values = new String[]{tuple.getStringByField("timestamp"), tuple.getStringByField("payload")};
                stateMap.put(tuple.getStringByField("deviceId"), values);
            }
        } else {
            // flush the accumulator cache by emitting new tuples
            for (Map.Entry<String, String[]> entry : stateMap.entrySet()) {
                // unanchored ... we don't care if they pop or not .. more will come in their wake
                outputCollector.emit(new Values(entry.getKey(), entry.getValue()[0], entry.getValue()[1]));
            }
        }
        // acknowledge the tuple as having been processed
        outputCollector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("deviceId", "timestamp", "payload"));
    }

    private static boolean isTickTuple(Tuple tuple) {
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
                && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        // emit ticks for this component once every 3 seconds
        int tickFrequencyInSeconds = 3;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }
}
