import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Performs a basic load test.
 *
 * Created by radu on 14/09/15.
 */
public class LoadTester {

    public static void main(String[] args) throws Exception {
        InputStream in = LoadTester.class.getClassLoader().getResourceAsStream("config.properties");
        Properties properties = new Properties();
        properties.load(in);

        int deviceNumber = Integer.valueOf(properties.getProperty("device.total"));
        String devicePrefix = properties.getProperty("device.prefix");
        final String serverUrl = properties.getProperty("server.url");
        Map<Integer, Integer> offlineMap = new HashMap<Integer, Integer>();
        String[] deviceArgs = properties.getProperty("offline").split(",");
        for (String deviceArg : deviceArgs) {
            String[] deviceConfig = deviceArg.split(":");
            offlineMap.put(Integer.valueOf(deviceConfig[0]), Integer.valueOf(deviceConfig[1]));
        }
        final List<String> devices = new ArrayList<String>();
        for (int i = 0; i < deviceNumber; i++) {
            devices.add(devicePrefix + i);
        }

        // equal with the number of cores
        ExecutorService executor = Executors.newFixedThreadPool(4);
        long startTime = System.currentTimeMillis();

        while (true) {
            for (int i = 0; i < deviceNumber; i++) {

                final String device = devices.get(i);

                // stop emitting for devices in offline map
                boolean stopDeviceFromEmitting = false;
                if (offlineMap.get(i) != null) {
                    long currentTime = System.currentTimeMillis();
                    long timeSpan = offlineMap.get(i) * 1000;
                    if (currentTime - startTime > timeSpan) {
                        stopDeviceFromEmitting = true;
                    }
                }

                if (!stopDeviceFromEmitting) {
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject obj = new JSONObject();
                            obj.put("deviceId", device);
                            obj.put("timestamp", System.currentTimeMillis());
                            obj.put("payload", UUID.randomUUID().toString());

                            try {
                                Request.Post(serverUrl).bodyString(obj.toString(), ContentType.APPLICATION_JSON)
                                        .execute().returnContent();
                                System.out.println("Posted telemetry signal = " + obj.toString());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }

            // simulate sleep once every second
            Thread.sleep(1000);
        }
    }
}
